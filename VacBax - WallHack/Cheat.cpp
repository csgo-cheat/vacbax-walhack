#include "stdafx.h"
#include "Cheat.h"
#include "offsets.h"
#include "Globals.h"

using namespace Globals;

HANDLE PROCESS_HANDLER;
DWORD PROCESS_DLL, ENGINE_DLL, PROCESS_ID;

void lanuch(char* name)
{
	FUNCTION_PROCESS_HANDLER(name);
	PROCESS_DLL = Module("client.dll");
	ENGINE_DLL = Module("engine.dll");
	HANDLE Threads[] =
	{
		CreateThread(nullptr, 0, reinterpret_cast<LPTHREAD_START_ROUTINE>(FUNCTION_GLOW_LOOP), nullptr, 0, nullptr),

	};

	WaitForMultipleObjects(sizeof(Threads) / sizeof(HANDLE), Threads, TRUE, INFINITE);
}

void FUNCTION_PROCESS_HANDLER(char *PARAMETR_PROCCESS_NAME)
{
	PROCESSENTRY32 PROCESS_ENTRY;
	PROCESS_ENTRY.dwSize = sizeof(PROCESSENTRY32);

	const HANDLE PROCESS_SNAPSHOT = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
	if (Process32First(PROCESS_SNAPSHOT, &PROCESS_ENTRY))
	{
		do
		{
			if (!strcmp(PROCESS_ENTRY.szExeFile, PARAMETR_PROCCESS_NAME))
			{
				PROCESS_ID = PROCESS_ENTRY.th32ProcessID;
				PROCESS_HANDLER = OpenProcess(PROCESS_ALL_ACCESS, false, PROCESS_ID);
				break;
			}
		} while (Process32Next(PROCESS_SNAPSHOT, &PROCESS_ENTRY));
	}

	if (PROCESS_SNAPSHOT != INVALID_HANDLE_VALUE)
		CloseHandle(PROCESS_SNAPSHOT);
}

DWORD Module(LPCSTR PARAMETR_MODULE_NAME)
{
	MODULEENTRY32 MODULEE_ENTRY;
	MODULEE_ENTRY.dwSize = sizeof(MODULEENTRY32);

	DWORD OFFSET_MODULE_BASE_ADDRESS = 0;

	const HANDLE hMod = CreateToolhelp32Snapshot(TH32CS_SNAPMODULE, PROCESS_ID);
	if (Module32First(hMod, &MODULEE_ENTRY))
	{
		do
		{
			if (!strcmp(MODULEE_ENTRY.szModule, PARAMETR_MODULE_NAME))
			{
				OFFSET_MODULE_BASE_ADDRESS = reinterpret_cast<DWORD>(MODULEE_ENTRY.modBaseAddr);
				break;
			}
		} while (Module32Next(hMod, &MODULEE_ENTRY));
	}

	if (hMod != INVALID_HANDLE_VALUE)
		CloseHandle(hMod);

	return OFFSET_MODULE_BASE_ADDRESS;
}

void FUNCTION_GLOW_LOOP()
{
	STRUCT_GLOW_COLOR_SET settings(true, false, false);

	int VARIABLE_CLASSID = 0;
	bool VARIABLE_SLEEP = false;
	DWORD VARIABLE_BUFFER = 0;
	DWORD VARIABLE_ENTITY = 0;
	DWORD VARIABLE_ICBASE = 0;
	int VARIABLE_SELF_TEAM = 0;
	int VARIABLE_SELF_HEALTH = 0;
	int VARIABLE_ENEMY_TEAM = 0;
	int VARIABLE_ENEMY_HEALTH = 0;
	int VARIABLE_SPOTTED_BYMASK = 0;
	int VARIABLE_SPOTTED = 0;
	int VARIABLE_CROSSHAIR = 0;

	if (gVARIABLE_GLOW_ENABLED)
	{
		while (true)
		{
			DWORD VARIABLE_GLOW = 0;
			DWORD VARIABLE_GLOW_ENTITY = 0;

			ReadProcessMemory(PROCESS_HANDLER, reinterpret_cast<LPVOID>(PROCESS_DLL + offsets::netvar_object_manager), &VARIABLE_GLOW, sizeof(VARIABLE_GLOW), nullptr);

			if (!VARIABLE_GLOW)
				return;

			ReadProcessMemory(PROCESS_HANDLER, reinterpret_cast<LPVOID>(PROCESS_DLL + offsets::netvar_object_size), &VARIABLE_GLOW_ENTITY, sizeof(VARIABLE_GLOW_ENTITY), nullptr);


			for (int i = 0; i < VARIABLE_GLOW_ENTITY; i++)
			{

				ReadProcessMemory(PROCESS_HANDLER, reinterpret_cast<LPVOID>(VARIABLE_GLOW + (i * 0x38)), &VARIABLE_ENTITY, sizeof(VARIABLE_ENTITY), nullptr);

				if (!VARIABLE_ENTITY) continue;

				ReadProcessMemory(PROCESS_HANDLER, reinterpret_cast<LPVOID>(VARIABLE_ENTITY + offsets::sleep), &VARIABLE_SLEEP, sizeof(VARIABLE_SLEEP), nullptr);

				if (VARIABLE_SLEEP) continue;

				ReadProcessMemory(PROCESS_HANDLER, reinterpret_cast<LPVOID>(VARIABLE_ENTITY + 0x8), &VARIABLE_BUFFER, sizeof(VARIABLE_BUFFER), nullptr);

				if (!VARIABLE_BUFFER) continue;

				ReadProcessMemory(PROCESS_HANDLER, reinterpret_cast<LPVOID>(VARIABLE_BUFFER + 0x8), &VARIABLE_BUFFER, sizeof(VARIABLE_BUFFER), nullptr);

				if (!VARIABLE_BUFFER) continue;

				ReadProcessMemory(PROCESS_HANDLER, reinterpret_cast<LPVOID>(VARIABLE_BUFFER + 0x1), &VARIABLE_BUFFER, sizeof(VARIABLE_BUFFER), nullptr);

				if (!VARIABLE_BUFFER) continue;

				ReadProcessMemory(PROCESS_HANDLER, reinterpret_cast<LPVOID>(VARIABLE_BUFFER + 0x14), &VARIABLE_CLASSID, sizeof(VARIABLE_CLASSID), nullptr);

				if (!VARIABLE_CLASSID) continue;

				ReadProcessMemory(PROCESS_HANDLER, reinterpret_cast<LPVOID>(PROCESS_DLL + offsets::netvar_self_player), &VARIABLE_ICBASE, sizeof(VARIABLE_ICBASE), nullptr);

				if (!VARIABLE_ICBASE) continue;

				ReadProcessMemory(PROCESS_HANDLER, reinterpret_cast<LPVOID>(VARIABLE_ICBASE + offsets::self_team), &VARIABLE_SELF_TEAM, sizeof(VARIABLE_SELF_TEAM), nullptr);

				ReadProcessMemory(PROCESS_HANDLER, reinterpret_cast<LPVOID>(VARIABLE_ICBASE + offsets::self_health), &VARIABLE_SELF_HEALTH, sizeof(VARIABLE_SELF_HEALTH), nullptr);

				ReadProcessMemory(PROCESS_HANDLER, reinterpret_cast<LPVOID>(VARIABLE_ENTITY + offsets::self_team), &VARIABLE_ENEMY_TEAM, sizeof(VARIABLE_ENEMY_TEAM), nullptr);

				ReadProcessMemory(PROCESS_HANDLER, reinterpret_cast<LPVOID>(VARIABLE_ENTITY + offsets::self_health), &VARIABLE_ENEMY_HEALTH, sizeof(VARIABLE_ENEMY_HEALTH), nullptr);


				switch (VARIABLE_CLASSID)
				{
					// people
				case 35:

					if (!gPLAYER_GLOW_ENABLED)
						return;

					if (VARIABLE_SELF_TEAM == VARIABLE_ENEMY_TEAM)
						break;

					ReadProcessMemory(PROCESS_HANDLER, reinterpret_cast<LPVOID>(VARIABLE_ENTITY + offsets::spotted_bymask), &VARIABLE_SPOTTED_BYMASK,
						sizeof(VARIABLE_SPOTTED_BYMASK), nullptr); // is user VARIABLE_SPOTTED by mask to any team mate and me in match (what is mask)

					ReadProcessMemory(PROCESS_HANDLER, reinterpret_cast<LPVOID>(VARIABLE_ENTITY + offsets::spotted), &VARIABLE_SPOTTED,
						sizeof(VARIABLE_SPOTTED), nullptr); // is user VARIABLE_SPOTTED to any team mate and me in match

					ReadProcessMemory(PROCESS_HANDLER, reinterpret_cast<LPVOID>(VARIABLE_ENTITY + offsets::crosshair), &VARIABLE_CROSSHAIR,
						sizeof(VARIABLE_CROSSHAIR), nullptr); // is OFFSET_CROSSHAIR is on enemy


					if (VARIABLE_SPOTTED_BYMASK || VARIABLE_CROSSHAIR || VARIABLE_SPOTTED) {

						WriteProcessMemory(PROCESS_HANDLER, reinterpret_cast<LPVOID>(VARIABLE_GLOW + (i * 0x38) + 0x4), &gPLAYER_COLOR, sizeof(gPLAYER_COLOR), nullptr);
						WriteProcessMemory(PROCESS_HANDLER, reinterpret_cast<LPVOID>(VARIABLE_GLOW + (i * 0x38) + 0x24), &settings, sizeof(settings), nullptr);

					}
					else {

						STRUCT_GLOW_COLOR color(float((255 - 2.55 * VARIABLE_ENEMY_HEALTH) / 255.0f), float((2.55 * VARIABLE_ENEMY_HEALTH) / 255.0f), 0, 1);

						WriteProcessMemory(PROCESS_HANDLER, reinterpret_cast<LPVOID>(VARIABLE_GLOW + (i * 0x38) + 0x4), &color, sizeof(color), nullptr);
						WriteProcessMemory(PROCESS_HANDLER, reinterpret_cast<LPVOID>(VARIABLE_GLOW + (i * 0x38) + 0x24), &settings, sizeof(settings), nullptr);

					}

					break;

					// bomb
				case 29:
				case 108:

					if (!gBOOMB_GLOW_ENABLED)
						break;

					WriteProcessMemory(PROCESS_HANDLER, reinterpret_cast<LPVOID>(VARIABLE_GLOW + (i * 0x38) + 0x4), &gBOOMB_COLOR, sizeof(gBOOMB_COLOR), nullptr);
					WriteProcessMemory(PROCESS_HANDLER, reinterpret_cast<LPVOID>(VARIABLE_GLOW + (i * 0x38) + 0x24), &settings, sizeof(settings), nullptr);

					break;

					// nades
				case 8:  case 9:  case 13:  case 40:
				case 41: case 64: case 82:	case 85:
				case 93: case 94: case 126:	case 127:
				// case 240: is smoke but is also defined in guns + some of these are "Projectile"

					if (!gNADE_GLOW_ENABLED)
						break;

					WriteProcessMemory(PROCESS_HANDLER, reinterpret_cast<LPVOID>(VARIABLE_GLOW + (i * 0x38) + 0x4), &gNADE_COLOR, sizeof(gNADE_COLOR), nullptr);
					WriteProcessMemory(PROCESS_HANDLER, reinterpret_cast<LPVOID>(VARIABLE_GLOW + (i * 0x38) + 0x24), &settings, sizeof(settings), nullptr);

					break;

					// guns
				case 1:
				case 39:
				//case 204 ... 244:
				// since VS don't accept "..." we  use some JS or maybe python or bash to generate some shit
				// for (j=1,i = 204; i <= 244; i++,j++) { process.stdout.write("case " + i + ": "); if (j % 5 == 0) console.log(""); }
				// EZ :) (Don't Be MSHF)
				
				case 204: case 205: case 206: case 207: case 208:
				case 209: case 210: case 211: case 212: case 213:
				case 214: case 215: case 216: case 217: case 218:
				case 219: case 220: case 221: case 222: case 223:
				case 224: case 225: case 226: case 227: case 228:
				case 229: case 230: case 231: case 232: case 233:
				case 234: case 235: case 236: case 237: case 238:
				case 239: case 240: case 241: case 242: case 243:
				case 244:

					if (!gGUN_GLOW_ENABLED)
						break;

					WriteProcessMemory(PROCESS_HANDLER, reinterpret_cast<LPVOID>(VARIABLE_GLOW + (i * 0x38) + 0x4), &gGUN_COLOR, sizeof(gGUN_COLOR), nullptr);
					WriteProcessMemory(PROCESS_HANDLER, reinterpret_cast<LPVOID>(VARIABLE_GLOW + (i * 0x38) + 0x24), &settings, sizeof(settings), nullptr);

					break;
				default: ;
				}

			}
		}
	}
}