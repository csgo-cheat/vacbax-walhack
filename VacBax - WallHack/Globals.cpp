#include "stdafx.h"
#include "Globals.h"


STRUCT_GLOW_COLOR Globals::gCOLOR_ORANGE(1, 0.5f, 0, 0.8f);
STRUCT_GLOW_COLOR Globals::gCOLOR_YELLOW(1, 1, 0, 0.8f);
STRUCT_GLOW_COLOR Globals::gCOLOR_BLUE(0, 0, 1, 0.8f);
STRUCT_GLOW_COLOR Globals::gCOLOR_RED(1, 0, 0, 0.8f);
STRUCT_GLOW_COLOR Globals::gCOLOR_GREEN(0, 1, 0, 0.8f);
STRUCT_GLOW_COLOR Globals::gCOLOR_WHITE(1, 1, 1, 0.8f);
STRUCT_GLOW_COLOR Globals::gCOLOR_BLACK(0, 0, 0, 0.8f);
STRUCT_GLOW_COLOR Globals::gCOLOR_PURPLE(0.5f, 0, 1, 0.8f);
STRUCT_GLOW_COLOR Globals::gCOLOR_PINK(1, 0, 0.5f, 0.8f);

bool Globals::gVARIABLE_GLOW_ENABLED = true;

bool Globals::gPLAYER_GLOW_ENABLED = true;
bool Globals::gBOOMB_GLOW_ENABLED = true;
bool Globals::gNADE_GLOW_ENABLED = true;
bool Globals::gGUN_GLOW_ENABLED = true;

STRUCT_GLOW_COLOR Globals::gPLAYER_COLOR = Globals::gCOLOR_BLUE;
STRUCT_GLOW_COLOR Globals::gBOOMB_COLOR = Globals::gCOLOR_RED;
STRUCT_GLOW_COLOR Globals::gNADE_COLOR = Globals::gCOLOR_ORANGE;
STRUCT_GLOW_COLOR Globals::gGUN_COLOR = Globals::gCOLOR_YELLOW;
