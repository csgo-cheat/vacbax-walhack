#pragma once
#include "stdafx.h"

struct STRUCT_GLOW_COLOR
{
	STRUCT_GLOW_COLOR(float PARAMETR_RED_COLOR, float PARAMETR_GREEN_COLOR, float PARAMETR_BLUE_COLOR,
		float PARAMETR_ADUNNO_COLOR)
	{
		VARIABLE_RED_COLOR = PARAMETR_RED_COLOR; VARIABLE_GREEN_COLOR = PARAMETR_GREEN_COLOR; VARIABLE_BLUE_COLOR = PARAMETR_BLUE_COLOR; VARIABLE_ADUNNO_COLOR = PARAMETR_ADUNNO_COLOR;
	}
	float VARIABLE_RED_COLOR, VARIABLE_GREEN_COLOR, VARIABLE_BLUE_COLOR, VARIABLE_ADUNNO_COLOR;
};

struct STRUCT_GLOW_COLOR_SET
{
	STRUCT_GLOW_COLOR_SET(bool _a1, bool _a2, bool _a3)
	{
		a1 = _a1; a2 = _a2; a3 = _a3;
	}
	bool a1, a2, a3;
};

namespace Globals
{
	extern bool gVARIABLE_GLOW_ENABLED;
	
	extern STRUCT_GLOW_COLOR gCOLOR_ORANGE;
	extern STRUCT_GLOW_COLOR gCOLOR_YELLOW;
	extern STRUCT_GLOW_COLOR gCOLOR_BLUE;
	extern STRUCT_GLOW_COLOR gCOLOR_RED;
	extern STRUCT_GLOW_COLOR gCOLOR_GREEN;
	extern STRUCT_GLOW_COLOR gCOLOR_WHITE;
	extern STRUCT_GLOW_COLOR gCOLOR_BLACK;
	extern STRUCT_GLOW_COLOR gCOLOR_PURPLE;
	extern STRUCT_GLOW_COLOR gCOLOR_PINK;

	extern bool gPLAYER_GLOW_ENABLED;
	extern bool gBOOMB_GLOW_ENABLED;
	extern bool gNADE_GLOW_ENABLED;
	extern bool gGUN_GLOW_ENABLED;

	extern STRUCT_GLOW_COLOR gPLAYER_COLOR;
	extern STRUCT_GLOW_COLOR gBOOMB_COLOR;
	extern STRUCT_GLOW_COLOR gNADE_COLOR;
	extern STRUCT_GLOW_COLOR gGUN_COLOR;

};