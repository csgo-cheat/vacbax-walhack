// VacBax - WallHack.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "VacBax - WallHack.h"
#include "Cheat.h"
#include "Globals.h"

using namespace Globals;

#define MAX_LOADSTRING 100

#define BUTTON_LUANCH 1001

#define CHECKBOX_ENABLE 2001
#define CHECKBOX_GUN 2002
#define CHECKBOX_BOOMB 2003
#define CHECKBOX_PLAYER 2004
#define CHECKBOX_NADE 2005

#define COMBO_NADE 3001
#define COMBO_GUN 3002
#define COMBO_BOOMB 3003
#define COMBO_PLAYER 3004

// Global Variables:
HINSTANCE hInst;                                // current instance
WCHAR szTitle[MAX_LOADSTRING];                  // The title bar text
WCHAR szWindowClass[MAX_LOADSTRING];            // the main window class name

// Forward declarations of functions included in this code module:
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPWSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

    // TODO: Place code here.

    // Initialize global strings
    LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
    LoadStringW(hInstance, IDC_VACBAXWALLHACK, szWindowClass, MAX_LOADSTRING);
    MyRegisterClass(hInstance);

    // Perform application initialization:
    if (!InitInstance (hInstance, nCmdShow))
    {
        return FALSE;
    }

    HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_VACBAXWALLHACK));

    MSG msg;

    // Main message loop:
    while (GetMessage(&msg, nullptr, 0, 0))
    {
        if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
    }

    return (int) msg.wParam;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
    WNDCLASSEXW wcex;

    wcex.cbSize = sizeof(WNDCLASSEX);

    wcex.style          = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc    = WndProc;
    wcex.cbClsExtra     = 0;
    wcex.cbWndExtra     = 0;
    wcex.hInstance      = hInstance;
    wcex.hIcon          = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_VACBAXWALLHACK));
    wcex.hCursor        = LoadCursor(nullptr, IDC_ARROW);
    wcex.hbrBackground  = reinterpret_cast<HBRUSH>(COLOR_WINDOW + 1);
    wcex.lpszMenuName   = nullptr;
    wcex.lpszClassName  = szWindowClass;
    wcex.hIconSm        = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

    return RegisterClassExW(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   hInst = hInstance; // Store instance handle in our global variable

   const HWND hWnd = CreateWindowW(szWindowClass, szTitle, WS_OVERLAPPED | WS_CAPTION | WS_SYSMENU | WS_MINIMIZEBOX |
	   WS_MAXIMIZEBOX,
	   CW_USEDEFAULT, 0, 600, 400, nullptr, nullptr, hInstance, nullptr);

   if (!hWnd)
   {
      return FALSE;
   }

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}
//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND  - process the application menu
//  WM_PAINT    - Paint the main window
//  WM_DESTROY  - post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	char name[] = { 99 , 115 , 103 , 111 , '.', 'e', 'x', 'e', '\0' };

	const char *Colors[] = { "Orange", "Yellow", "Blue", "Red", "Green", "White", "Black", "Purple", "Pink" };

    switch (message)
    {
	case WM_CREATE:
	{
		const HWND btn_start = CreateWindow(TEXT("button"), TEXT("Start"),
			WS_VISIBLE | WS_CHILD,
			20, 20, 80, 25,
			hWnd, (HMENU)BUTTON_LUANCH, NULL, NULL);

		const HWND check_box_enable = CreateWindow(TEXT("button"), TEXT("Enable"),
			WS_VISIBLE | WS_CHILD | BS_AUTOCHECKBOX,
			20, 55, 150, 25,
			hWnd, (HMENU)CHECKBOX_ENABLE, NULL, NULL);

		const HWND hWndComboBoxPlayer = CreateWindow("COMBOBOX", NULL,
			WS_VISIBLE | WS_CHILD | CBS_DROPDOWNLIST,
			175, 85, 150, 800,
			hWnd, (HMENU)COMBO_PLAYER, hInst, NULL);

		const HWND check_box_player = CreateWindow(TEXT("button"), TEXT("Glow Player"),
			WS_VISIBLE | WS_CHILD | BS_AUTOCHECKBOX,
			20, 85, 150, 25,
			hWnd, (HMENU)CHECKBOX_PLAYER, NULL, NULL);

		const HWND hWndComboBoxNade = CreateWindow("COMBOBOX", NULL,
			WS_VISIBLE | WS_CHILD | CBS_DROPDOWNLIST,
			175, 115, 150, 800,
			hWnd, (HMENU)COMBO_NADE, hInst, NULL);

		const HWND check_box_nade = CreateWindow(TEXT("button"), TEXT("Glow Nade"),
			WS_VISIBLE | WS_CHILD | BS_AUTOCHECKBOX,
			20, 115, 150, 25,
			hWnd, (HMENU)CHECKBOX_NADE, NULL, NULL);

		const HWND hWndComboBoxGun = CreateWindow("COMBOBOX", NULL,
			WS_VISIBLE | WS_CHILD | CBS_DROPDOWNLIST,
			175, 145, 150, 800,
			hWnd, (HMENU)COMBO_GUN, hInst, NULL);

		const HWND check_box_gun = CreateWindow(TEXT("button"), TEXT("Glow Gun"),
			WS_VISIBLE | WS_CHILD | BS_AUTOCHECKBOX,
			20, 145, 150, 25,
			hWnd, (HMENU)CHECKBOX_GUN, NULL, NULL);
			
		const HWND hWndComboBoxBoomb = CreateWindow("COMBOBOX", NULL,
			WS_VISIBLE | WS_CHILD | CBS_DROPDOWNLIST,
			175, 175, 150, 800,
			hWnd, (HMENU)COMBO_BOOMB, hInst, NULL);

		const HWND check_box_boomb = CreateWindow(TEXT("button"), TEXT("Glow Boomb"),
			WS_VISIBLE | WS_CHILD | BS_AUTOCHECKBOX,
			20, 175, 150, 25,
			hWnd, (HMENU)CHECKBOX_BOOMB, NULL, NULL);

		// initilize ComboBoxS items
		for (auto color : Colors) {
			SendMessage(hWndComboBoxGun, CB_ADDSTRING, 0, reinterpret_cast<LPARAM>(color));
			SendMessage(hWndComboBoxNade, CB_ADDSTRING, 0, reinterpret_cast<LPARAM>(color));
			SendMessage(hWndComboBoxPlayer, CB_ADDSTRING, 0, reinterpret_cast<LPARAM>(color));
			SendMessage(hWndComboBoxBoomb, CB_ADDSTRING, 0, reinterpret_cast<LPARAM>(color));
		}

		// pre check CheckBoxes
		CheckDlgButton(hWnd, CHECKBOX_BOOMB, true);
		CheckDlgButton(hWnd, CHECKBOX_ENABLE, true);
		CheckDlgButton(hWnd, CHECKBOX_GUN, true);
		CheckDlgButton(hWnd, CHECKBOX_NADE, true);
		CheckDlgButton(hWnd, CHECKBOX_PLAYER, true);

		// pre set combo box item
		SendMessage(hWndComboBoxGun, CB_SETCURSEL, 1, 0);
		SendMessage(hWndComboBoxNade, CB_SETCURSEL, 0, 0);
		SendMessage(hWndComboBoxPlayer, CB_SETCURSEL, 2, 0);
		SendMessage(hWndComboBoxBoomb, CB_SETCURSEL, 3, 0);
	}
	break;
    case WM_COMMAND:
        {
            int wmId = LOWORD(wParam);
            switch (wmId)
            {

			case COMBO_PLAYER:
			{
				HandleComboBox(COMBO_PLAYER, wParam, hWnd);
				break;
			}
			case COMBO_GUN:
			{
				HandleComboBox(COMBO_GUN, wParam, hWnd);
				break;
			}
			case COMBO_BOOMB:
			{
				HandleComboBox(COMBO_BOOMB, wParam, hWnd);
				break;
			}
			case COMBO_NADE:
			{
				HandleComboBox(COMBO_NADE, wParam, hWnd);
				break;
			}

			case CHECKBOX_ENABLE:
			{
				if (HIWORD(wParam) == BN_CLICKED)
					gVARIABLE_GLOW_ENABLED = SendDlgItemMessage(hWnd, CHECKBOX_ENABLE, BM_GETCHECK, 0, 0);

				break;
			}

			case CHECKBOX_PLAYER:
			{
				if (HIWORD(wParam) == BN_CLICKED)
					gPLAYER_GLOW_ENABLED = SendDlgItemMessage(hWnd, CHECKBOX_PLAYER, BM_GETCHECK, 0, 0);

				break;
			}
			case CHECKBOX_GUN:
			{
				if (HIWORD(wParam) == BN_CLICKED)
					gGUN_GLOW_ENABLED = SendDlgItemMessage(hWnd, CHECKBOX_GUN, BM_GETCHECK, 0, 0);

				break;
			}
			case CHECKBOX_NADE:
			{
				if (HIWORD(wParam) == BN_CLICKED)
					gNADE_GLOW_ENABLED = SendDlgItemMessage(hWnd, CHECKBOX_NADE, BM_GETCHECK, 0, 0);

				break;
			}
			case CHECKBOX_BOOMB:
			{
				if (HIWORD(wParam) == BN_CLICKED)
					gBOOMB_GLOW_ENABLED = SendDlgItemMessage(hWnd, CHECKBOX_BOOMB, BM_GETCHECK, 0, 0);

				break;
			}
				
			case BUTTON_LUANCH:
				lanuch(name);

				break;
            default:
                return DefWindowProcW(hWnd, message, wParam, lParam);
            }
        }
        break;
    case WM_PAINT:
        {
            PAINTSTRUCT ps;
            HDC hdc = BeginPaint(hWnd, &ps);
            // TODO: Add any drawing code that uses hdc here...
            EndPaint(hWnd, &ps);
        }
        break;
    case WM_DESTROY:
        PostQuitMessage(0);
        break;
    default:
        return DefWindowProcW(hWnd, message, wParam, lParam);
    }
    return 0;
}

void HandleComboBox(int id, WPARAM wParam, HWND hWnd)
{
	if (HIWORD(wParam) == CBN_SELCHANGE)
	{
		int idx_row = SendMessage(GetDlgItem(hWnd, id), CB_GETCURSEL, 0, 0);
		STRUCT_GLOW_COLOR color = getColorByIndex(idx_row);

		switch (id)
		{
		case COMBO_PLAYER:
			gPLAYER_COLOR = color;
			break;
		case COMBO_BOOMB:
			gBOOMB_COLOR = color;
			break;
		case COMBO_NADE:
			gNADE_COLOR = color;
			break;
		case COMBO_GUN:
			gGUN_COLOR = color;
			break;
		default:
			break;
		}
	}
}

STRUCT_GLOW_COLOR getColorByIndex(int index)
{
	switch (index)
	{
		case 0:
			return gCOLOR_ORANGE;
		case 1:
			return gCOLOR_YELLOW;
		case 2:
			return gCOLOR_BLUE;
		case 3:
			return gCOLOR_RED;
		case 4:
			return gCOLOR_GREEN;
		case 5:
			return gCOLOR_WHITE;
		case 6:
			return gCOLOR_BLACK;
		case 7:
			return gCOLOR_PURPLE;
		case 8:
			return gCOLOR_PINK;
		default:
			return gCOLOR_BLACK;
	}
}