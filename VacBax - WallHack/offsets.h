#pragma once

#include "stdafx.h"

namespace offsets
{
	inline extern DWORD self_health = 0xFC;
	inline extern DWORD self_team = 0xF0;
	inline extern DWORD sleep = 0xE9;
	inline extern DWORD spotted = 0x939;
	inline extern DWORD spotted_bymask = 0x97C;
	inline extern DWORD crosshair = 0xB2A4;
	inline extern DWORD netvar_object_manager = 0x4F9D2B8;
	inline extern DWORD netvar_object_size = netvar_object_manager + 0xC;
	inline extern DWORD netvar_self_player = 0xAA3154;
};