// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"

#define APP_VERSION "1"

#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers
// Windows Header Files:
#include <windows.h>

// C RunTime Header Files
#include <stdlib.h>
#include <malloc.h>
#include <memory.h>
#include <tchar.h>

#include <intrin.h>
#include <string>
#include <atlstr.h>

#include <cstdint>

#include <Commdlg.h>
#include <sstream>

#include <tlhelp32.h>

#include <assert.h>
#include <excpt.h>
#include <signal.h>
#include <shlwapi.h>


// TODO: reference additional headers your program requires here
